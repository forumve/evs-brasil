#
# Create a new page for each vehicle
#
#

require 'json'

module Jekyll

  class JsonDataPage < Jekyll::Page
    def initialize(site, base, dir, id, object)
    	@site = site
    	@base = base
    	@dir = dir
    	@name = id + ".json"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "json.json")
	self.data["object"] = object
    end
  end
  
  class YamlDataPage < Jekyll::Page
    def initialize(site, base, dir, id, object)
    	@site = site
    	@base = base
    	@dir = dir
    	@name = id + ".yaml"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "yaml.yaml")
	self.data["object"] = object
    end
  end
  


  class VehiclePage < Jekyll::Page
    def initialize(site, base, id, vehicle)
    	@site = site
    	@base = base
    	@dir = "vehicles"
    	@name = id + ".html"
    	self.process(@name)
	self.read_yaml(File.join(base, '_layouts'), "vehicle.html")
    	self.data["title"] = vehicle["name"] + " " + vehicle["my"].to_s
    	self.data["layout"] = "vehicle"
	self.data["vehicle"] = vehicle
	self.data["vehicle-id"] = id
    end
  end

  class VehicleGenerator < Jekyll::Generator


    def convert(vehicle, from, to, multiplier, digits)
    	if not vehicle[to] and vehicle[from]
    		vehicle[to] = (vehicle[from] * multiplier).round(digits)
    	end    
    end
   
    def kgf_to_n(vehicle, kgf, n)
    	convert(vehicle, kgf, n, 9.806650, 0)
    end

    def n_to_kgf(vehicle, n, kgf)
    	convert(vehicle, n, kgf, 1/9.806650, 0)
    end

    def w_to_hp(vehicle, w, hp)
    	convert(vehicle, w, hp, 0.00134102, 0)
    end

    def hp_to_w(vehicle, hp, w)
    	convert(vehicle, hp, w, (1/0.00134102), 0)
    end

    def w_to_cv(vehicle, w, cv)
    	convert(vehicle, w, cv, 0.00135961, 0)
    end

    def cv_to_w(vehicle, cv, w)
    	convert(vehicle, cv, w, (1/0.00135961), 0)
    end

    # Tratamento da hierarquia de definicoes
    def process_base_model(vehicles, vehicle)
        if vehicle['base-model'] and not vehicle['base-model-processed']
            # Marca que ja processou para garantir que nao fique em loop
            vehicle['base-model-processed'] = true
#            print(vehicle)
            base = vehicles[vehicle['base-model']]
            if base
                process_base_model(vehicles, base)
                base.each do |id,data|
                    if not vehicle[id] and [id] != "virtual"
                       vehicle[id] = data
                    end
                end
            else
                print("unknown base model " + vehicle['base-model'])
            end
        end
    end
    
    def generate(site)
    	vehicles = site.data["vehicles"]
    	path = "_site/data"
    	FileUtils.mkpath(path) unless File.exist?(path)
	vehicleIds = []
	vehicleArray = []
        cycles = [ "epa", "wltp", "ndec", "pbev" ]	
        
        # Processa primeiro hierarquia de definicoes para reaproveitar dados
	vehicles.each do |id,vehicle|
	     process_base_model(vehicles, vehicle)
	end
	
	vehicles.each do |id,vehicle|
                if vehicle['virtual'] == "true"
                    next
                end
		
		w_to_cv(vehicle, "ev-engine-power-w", "ev-engine-power-cv")
		cv_to_w(vehicle, "ev-engine-power-cv", "ev-engine-power-w")
		convert(vehicle, "ev-engine-power-w", "ev-engine-power-kw", 0.001, 2)
		convert(vehicle, "ev-engine-power-cv", "ev-engine-power-hp", 1.0139, 0)
		
		kgf_to_n(vehicle, "ev-engine-torque-n", "ev-engine-torque-kgf")
		kgf_to_n(vehicle, "ev-engine-torque-kgf", "ev-engine-torque-n")
		kgf_to_n(vehicle, "ice-engine-torque-kgf", "ice-engine-torque-n")
		kgf_to_n(vehicle, "combined-engine-torque-kgf", "combined-engine-torque-")
		
		if vehicle["weight"] and vehicle["ev-engine-power-cv"]
			vehicle["ev-weight-power-cv"] = ( ( 1.0 * vehicle["weight"] ) / vehicle["ev-engine-power-cv"] ).round(2)
		end
		
		if vehicle["weight"] and vehicle["ev-engine-power-kw"]
			vehicle["ev-weight-power-kw"] = ( ( 1.0 * vehicle["weight"] ) / vehicle["ev-engine-power-kw"] ).round(2)
		end
		
		if not vehicle["battery"] and vehicle["battery-packs"] and vehicle["battery-pack"]
			vehicle["battery"] = vehicle["battery-packs"] * vehicle["battery-pack"]
		end
		
		if not vehicle["battery"] and vehicle["battery-v"] and vehicle["battery-ah"]
			vehicle["battery"] = vehicle["battery-v"] * vehicle["battery-ah"]
		end

		# Se nao tem definicao de total utilizavel assume que eh total
		convert(vehicle, "battery", "battery-usable", 1, 0)
		
# https://insideevs.com/features/343231/heres-how-to-calculate-conflicting-ev-range-test-cycles-epa-wltp-nedc/
		convert(vehicle, "range-wltp", "range-epa", (1/1.12), 0)
		convert(vehicle, "range-ndec", "range-epa", (1/1.43), 0)
		convert(vehicle, "range-epa", "range-wltp", 1.12, 0)
		convert(vehicle, "range-epa", "range-ndec", 1.43, 0)
                if vehicle["pbev-consumo-energetico"] 
                    # MJ/km -> Wh/km - https://www.gov.br/inmetro/pt-br/acesso-a-informacao/perguntas-frequentes/avaliacao-da-conformidade/etiquetagem-para-veiculos-leves/como-e-calculado-o-consumo-dos-carros-eletricos
                    convert(vehicle, "pbev-consumo-energetico", "pbev-consumo-eletrico", (1/0.0036), 3)
                    
                    if vehicle["battery-usable"] 
                       # aplicando fator de 30% de reducao se utilizar somente consumo energetico
                       # Isso não esta claro, entendo que os 30% fazem parte do J1634 aqui fica assim 
                       # para comparacoes entre os valores
	               vehicle["range-pbev-pelo-consumo-energetico"] = ( (vehicle["battery-usable"] / vehicle["pbev-consumo-eletrico"]) * 0.7).round(0)
                        if not vehicle["range-pbev"]
                            vehicle["range-pbev"] = vehicle["range-pbev-pelo-consumo-energetico"]
		        end
		    end
               	end
               	# EPA e PEBV deveriam ter os valores praticamente iguais
#		convert(vehicle, "range-pbev", "range-epa", 1, 0)
#		convert(vehicle, "range-epa", "range-pbev", 1, 0)
		
		if vehicle["battery-usable"] 

                        convert(vehicle, "battery-usable", "battery-usable-kwh", 1/1000.0, 1)
                        convert(vehicle, "battery", "battery-kwh", 1/1000.0, 1)
                        vehicle["battery-buffer"] = vehicle["battery"] - vehicle["battery-usable"]
                        
                        convert(vehicle, "battery-buffer", "battery-buffer-kwh", 1/1000.0, 1)

			if vehicle["range"]
    			    vehicle["consumption-kwh-100km"] = (( ( vehicle["battery-usable"] / 1000.0) / vehicle["range"] ) * 100).round(2)
			end
			
			if vehicle["range-epa"]
			    for cycle in cycles do
			        if vehicle["range-"+cycle]
    			            vehicle["consumption-kwh-100km-" + cycle] = (( ( vehicle["battery-usable"] / 1000.0) / vehicle["range-" + cycle] ) * 100).round(2)
			            # MJ/km
			            convert(vehicle, "consumption-kwh-100km-" + cycle,  "consumption-mj-km-" + cycle, (3.6 / 100), 2)

			            if site.config["cost-br-kwh"] 
				       vehicle["cost-br-100km-" + cycle] =  ( vehicle["consumption-kwh-100km-" + cycle] * site.config["cost-br-kwh"]).round(2)
				    end
			        end
			    end

			end	
		
		end
		for cycle in cycles do
		    if vehicle["price-br"] and vehicle["range-" + cycle]
			vehicle["price-br-range-"+cycle] = ( (1.0 * vehicle["price-br"]) / vehicle["range-"+cycle]).round(2)
		    end	
		end
		
		site.pages.push(VehiclePage.new(site, site.source, id, vehicle))

		site.pages.push(JsonDataPage.new(site, site.source, "vehicles", id, vehicle ))
		site.pages.push(YamlDataPage.new(site, site.source, "vehicles", id, vehicle ))

		vehicleIds.push(id)
		vehicleArray.push(vehicle)
		
	end	    	
	site.pages.push(JsonDataPage.new(site, site.source, "vehicles", "vehicles-ids", vehicleIds))
	site.pages.push(JsonDataPage.new(site, site.source, "vehicles", "vehicles-array", vehicleArray))
	site.pages.push(JsonDataPage.new(site, site.source, "vehicles", "vehicles", vehicles))
	site.pages.push(YamlDataPage.new(site, site.source, "vehicles", "vehicles", vehicles))
    end

  end
  
end

