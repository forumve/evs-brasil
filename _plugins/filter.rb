module Jekyll
   module MyFilters
   
   
    def my_yamlify(input)
      as_liquid(input).to_yaml
    end

    def my_jsonify(input)
      JSON.pretty_generate(as_liquid(input))
    end

  end
end

Liquid::Template.register_filter(
  Jekyll::MyFilters
)
