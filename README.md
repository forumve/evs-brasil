![Build Status](https://gitlab.com/pages/jekyll/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)


Endereço de publicação : https://forumve.gitlab.io/evs-brasil/
---
Site com informações sobre veículos elétricos disponíveis no Brasil

Para incluir novos dados basta criar um novo arquivo no diretório
_data/vehicles de acordo com o formato dos arquivos existentes




Baseado no exemplo disponível no gitlab para utilização com jekyll

Para executar localmente :

```
bundle exec jekyll serve
```
